#!/bin/bash

echo `date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@` 
#kernel_name_release=`uname -s -r`
#name=`uname -n`
#machine=`uname -m`
#cpu_cores=`cat /proc/cpuinfo  | grep @doubleQuotation@cores@doubleQuotation@ |  wc -l`
#printf @doubleQuotation@%s %s (%s)     %s      _%s_    (%s CPU)\n\n@doubleQuotation@ $kernel_name_release $name $today $machine $cpu_cores  

# 获取容器自身ID
#CONTAINERID=$(cat /proc/self/cgroup | head -n 1| awk -F @singleQuotation@/@singleQuotation@ @singleQuotation@{print $3}@singleQuotation@)
# 获取每秒时钟计数 CLK_TCK
clk_tck=`getconf CLK_TCK`

for((i=1;i<=$2;i++))
do
printf @doubleQuotation@start_collecting@doubleQuotation@
#################### 获取CPU相关信息 ####################
# CPU使用详情
cd {cpuacct_path}
cpu_usage1=`printf @singleQuotation@@doubleQuotation@cpu_usage1@doubleQuotation@:{@doubleQuotation@cur_date@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cur_time@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@host_cpu_total@doubleQuotation@:%s, @doubleQuotation@container_cpu_usage@doubleQuotation@:%s, @doubleQuotation@containe_%s_cpu_usage@doubleQuotation@:%s, @doubleQuotation@container_%s_cpu_usage@doubleQuotation@:%s, @doubleQuotation@container_percup_usage@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@}\n@singleQuotation@ $(date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@;cat /proc/stat | grep @singleQuotation@cpu @singleQuotation@ | awk @singleQuotation@{print $2+$3+$4+$5+$6+$7+$8}@singleQuotation@;cat cpuacct.usage cpuacct.stat) @doubleQuotation@$(cat cpuacct.usage_percpu)@doubleQuotation@`


# 内存与交换空间限制详情
cd {memory_path}
mem_swap_failcnt1=`printf @singleQuotation@@doubleQuotation@mem_swap_failcnt1@doubleQuotation@:{@doubleQuotation@cur_date@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cur_time@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@mem_failcnt@doubleQuotation@:%s, @doubleQuotation@memsw_failcnt@doubleQuotation@:%s, @doubleQuotation@kmem_failcnt@doubleQuotation@:%s, @doubleQuotation@kmem_tcp_failcnt@doubleQuotation@:%s}@singleQuotation@ $(date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@;cat memory.failcnt  memory.memsw.failcnt memory.kmem.failcnt memory.kmem.tcp.failcnt)`

# 动态生成占位符
stat_item_num=`cat memory.stat | wc -l`
cmd_out=@singleQuotation@\@doubleQuotation@%s\@doubleQuotation@:%s@singleQuotation@
for((y=1;y<$stat_item_num;y++))
do
cmd_out=$cmd_out@singleQuotation@, \@doubleQuotation@%s\@doubleQuotation@:%s@singleQuotation@
done

mem_swap_usage1=`echo $cmd_out |xargs -I {} printf @singleQuotation@@doubleQuotation@mem_swap_usage1@doubleQuotation@:{@doubleQuotation@cur_date@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cur_time@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, {}}@singleQuotation@ $(date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@;cat memory.stat)`


# 块设备I/O详情
cd {blkio_path}

# 磁盘I/O使用详情
io_serviced=`printf @singleQuotation@@doubleQuotation@%s:%s:%s@doubleQuotation@,@singleQuotation@ $(cat blkio.throttle.io_serviced |grep -E @singleQuotation@Read|Write@singleQuotation@)`
if [ ${io_serviced%?} == @singleQuotation@@doubleQuotation@::@doubleQuotation@@singleQuotation@ ]; then
    io_serviced=`printf @singleQuotation@@doubleQuotation@%s:%s:%s@doubleQuotation@,@singleQuotation@ $(cat blkio.io_serviced | grep -E @singleQuotation@Read|Write@singleQuotation@)`
    if [ ${io_serviced%?} == @singleQuotation@@doubleQuotation@::@doubleQuotation@@singleQuotation@ ]; then
        io_serviced=@singleQuotation@@doubleQuotation@999:0:type:0@doubleQuotation@,@singleQuotation@
    fi
fi

io_service_bytes=`printf @singleQuotation@@doubleQuotation@%s:%s:%s@doubleQuotation@,@singleQuotation@ $(cat blkio.throttle.io_service_bytes|grep -E @singleQuotation@Read|Write@singleQuotation@)`
if [ ${io_service_bytes%?} == @singleQuotation@@doubleQuotation@::@doubleQuotation@@singleQuotation@ ]; then
    io_service_bytes=`printf @singleQuotation@@doubleQuotation@%s:%s:%s@doubleQuotation@,@singleQuotation@ $(cat blkio.io_service_bytes|grep -E @singleQuotation@Read|Write@singleQuotation@)`
    if [ ${io_service_bytes%?} == @singleQuotation@@doubleQuotation@::@doubleQuotation@@singleQuotation@ ]; then
        io_service_bytes=@singleQuotation@@doubleQuotation@999:0:type:0@doubleQuotation@,@singleQuotation@
    fi
fi


blkio1=`printf @singleQuotation@@doubleQuotation@blkio1@doubleQuotation@:{@doubleQuotation@cur_date@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cur_time@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@io_serviced@doubleQuotation@:[%s], @doubleQuotation@io_service_bytes@doubleQuotation@:[%s]\n}@singleQuotation@ $(date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@) @doubleQuotation@${io_serviced%?}@doubleQuotation@ @doubleQuotation@${io_service_bytes%?}@doubleQuotation@`


sleep $1


# 获取采样日期
#cur_date=`date @doubleQuotation@+%Y-%m-%d@doubleQuotation@`
# 获取采样时间
#cur_time=`date @doubleQuotation@+%H:%M:%S@doubleQuotation@`
#cur_time=`date --date=@singleQuotation@0 days ago@singleQuotation@ @doubleQuotation@+%H:%M:%S@doubleQuotation@` 

# CPU限制详情
cd {cpu_path}
printf @singleQuotation@{@doubleQuotation@cpu_limit@doubleQuotation@:{@doubleQuotation@cur_date@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cur_time@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cfs_period_us@doubleQuotation@:%s, @doubleQuotation@cfs_quota_us@doubleQuotation@:%s, @doubleQuotation@rt_period_us@doubleQuotation@:%s, @doubleQuotation@rt_runtime_us@doubleQuotation@:%s, @doubleQuotation@shares@doubleQuotation@:%s, @doubleQuotation@%s@doubleQuotation@:%s, @doubleQuotation@%s@doubleQuotation@:%s, @doubleQuotation@%s@doubleQuotation@:%s}}\n||@singleQuotation@ $(date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@;cat cpu.cfs_period_us cpu.cfs_quota_us cpu.rt_period_us cpu.rt_runtime_us cpu.shares cpu.stat)

# CPU使用详情
cd {cpuacct_path}
cpu_usage2=`printf @singleQuotation@@doubleQuotation@cpu_usage2@doubleQuotation@:{@doubleQuotation@cur_date@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cur_time@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@host_cpu_total@doubleQuotation@:%s, @doubleQuotation@container_cpu_usage@doubleQuotation@:%s, @doubleQuotation@containe_%s_cpu_usage@doubleQuotation@:%s, @doubleQuotation@container_%s_cpu_usage@doubleQuotation@:%s, @doubleQuotation@%s@doubleQuotation@:%s, @doubleQuotation@%s@doubleQuotation@:%s, @doubleQuotation@%s@doubleQuotation@:%s, @doubleQuotation@container_percup_usage@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@}\n@singleQuotation@ $(date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@; cat /proc/stat | grep @singleQuotation@cpu @singleQuotation@ | awk @singleQuotation@{print $2+$3+$4+$5+$6+$7+$8}@singleQuotation@;cat cpuacct.usage cpuacct.stat {cpu_path}/cpu.stat) @doubleQuotation@$(cat cpuacct.usage_percpu)@doubleQuotation@`

printf @singleQuotation@{@doubleQuotation@cpu_usage@doubleQuotation@:{@doubleQuotation@clk_tck@doubleQuotation@:%s, %s, %s}}||@singleQuotation@ $clk_tck @doubleQuotation@$cpu_usage1@doubleQuotation@ @doubleQuotation@$cpu_usage2@doubleQuotation@

#################### 获取内存相关信息 ####################
# 内存与交换空间限制详情
cd {memory_path}
mem_swap_limit=`printf @singleQuotation@@doubleQuotation@mem_swap_limit@doubleQuotation@:{@doubleQuotation@cur_date@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cur_time@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@mem_limit@doubleQuotation@:%s, @doubleQuotation@memsw_limit@doubleQuotation@:%s, @doubleQuotation@swappiness@doubleQuotation@:%s}@singleQuotation@ $(date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@;cat memory.limit_in_bytes  memory.memsw.limit_in_bytes memory.swappiness)`

mem_swap_failcnt2=`printf @singleQuotation@@doubleQuotation@mem_swap_failcnt2@doubleQuotation@:{@doubleQuotation@cur_date@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cur_time@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@mem_failcnt@doubleQuotation@:%s, @doubleQuotation@memsw_failcnt@doubleQuotation@:%s, @doubleQuotation@kmem_failcnt@doubleQuotation@:%s, @doubleQuotation@kmem_tcp_failcnt@doubleQuotation@:%s, @doubleQuotation@%s@doubleQuotation@:%s, @doubleQuotation@%s@doubleQuotation@:%s}@singleQuotation@ $(date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@;cat memory.failcnt  memory.memsw.failcnt memory.kmem.failcnt memory.kmem.tcp.failcnt memory.oom_control)`

# 内存与交换空间使用详情
host_mem_swap_total=`cat /proc/meminfo | grep -E @singleQuotation@MemTotal|SwapTotal@singleQuotation@ |awk -F @singleQuotation@ @singleQuotation@ {@singleQuotation@print $2@singleQuotation@}`

# 动态生成占位符
stat_item_num=`cat memory.stat | wc -l`
cmd_out=@singleQuotation@\@doubleQuotation@%s\@doubleQuotation@:%s@singleQuotation@
for((y=1;y<$stat_item_num;y++))
do
cmd_out=$cmd_out@singleQuotation@, \@doubleQuotation@%s\@doubleQuotation@:%s@singleQuotation@
done

mem_swap_usage2=`echo $cmd_out |xargs -I {} printf @singleQuotation@@doubleQuotation@mem_swap_usage2@doubleQuotation@:{@doubleQuotation@cur_date@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cur_time@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, {}, @doubleQuotation@mem_usage@doubleQuotation@:%s, @doubleQuotation@memsw_usage@doubleQuotation@:%s, @doubleQuotation@host_mem_total@doubleQuotation@:%s, @doubleQuotation@host_swap_total@doubleQuotation@:%s}@singleQuotation@ $(date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@;cat memory.stat memory.usage_in_bytes memory.memsw.usage_in_bytes; echo $host_mem_swap_total)`
#mem_swap_usage=`echo @singleQuotation@$cmd_out@singleQuotation@|xargs -I {} printf {} $(cat memory.stat)`
printf @singleQuotation@{@doubleQuotation@mem_swap@doubleQuotation@: {%s, %s, %s, %s, %s}}||@singleQuotation@ @doubleQuotation@$mem_swap_limit@doubleQuotation@  @doubleQuotation@$mem_swap_usage1@doubleQuotation@ @doubleQuotation@$mem_swap_failcnt1@doubleQuotation@ @doubleQuotation@$mem_swap_usage2@doubleQuotation@ @doubleQuotation@$mem_swap_failcnt2@doubleQuotation@



#################### 获取块设备I/O相关信息 ####################
cd {blkio_path}

# 磁盘I/O使用详情
io_serviced=`printf @singleQuotation@@doubleQuotation@%s:%s:%s@doubleQuotation@,@singleQuotation@ $(cat blkio.throttle.io_serviced |grep -E @singleQuotation@Read|Write@singleQuotation@)`
if [ ${io_serviced%?} == @singleQuotation@@doubleQuotation@::@doubleQuotation@@singleQuotation@ ]; then
    io_serviced=`printf @singleQuotation@@doubleQuotation@%s:%s:%s@doubleQuotation@,@singleQuotation@ $(cat blkio.io_serviced | grep -E @singleQuotation@Read|Write@singleQuotation@)`
    if [ ${io_serviced%?} == @singleQuotation@@doubleQuotation@::@doubleQuotation@@singleQuotation@ ]; then
        io_serviced=@singleQuotation@@doubleQuotation@999:0:type:0@doubleQuotation@,@singleQuotation@
    fi  
fi

io_service_bytes=`printf @singleQuotation@@doubleQuotation@%s:%s:%s@doubleQuotation@,@singleQuotation@ $(cat blkio.throttle.io_service_bytes|grep -E @singleQuotation@Read|Write@singleQuotation@)`
if [ ${io_service_bytes%?} == @singleQuotation@@doubleQuotation@::@doubleQuotation@@singleQuotation@ ]; then
    io_service_bytes=`printf @singleQuotation@@doubleQuotation@%s:%s:%s@doubleQuotation@,@singleQuotation@ $(cat blkio.io_service_bytes|grep -E @singleQuotation@Read|Write@singleQuotation@)`
    if [ ${io_service_bytes%?} == @singleQuotation@@doubleQuotation@::@doubleQuotation@@singleQuotation@ ]; then
        io_service_bytes=@singleQuotation@@doubleQuotation@999:0:type:0@doubleQuotation@,@singleQuotation@
    fi  
fi


blkio2=`printf @singleQuotation@@doubleQuotation@blkio2@doubleQuotation@:{@doubleQuotation@cur_date@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@cur_time@doubleQuotation@:@doubleQuotation@%s@doubleQuotation@, @doubleQuotation@io_serviced@doubleQuotation@:[%s], @doubleQuotation@io_service_bytes@doubleQuotation@:[%s]\n}@singleQuotation@ $(date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@) @doubleQuotation@${io_serviced%?}@doubleQuotation@ @doubleQuotation@${io_service_bytes%?}@doubleQuotation@`

printf @singleQuotation@{@doubleQuotation@blkio@doubleQuotation@:{%s, %s}}||@singleQuotation@ @doubleQuotation@$blkio1@doubleQuotation@ @doubleQuotation@$blkio2@doubleQuotation@

#################### 获取网络相关信息 ###################

echo @doubleQuotation@end_collecting@doubleQuotation@
done
echo `date @doubleQuotation@+%Y-%m-%d %H:%M:%S@doubleQuotation@`