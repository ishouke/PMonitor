#!/usr/bin/env/ python
# -*- coding:utf-8 -*-

__author__ = 'laifuyu'

import os
import subprocess

from log import logger

class OtherTools:
    def __init__(self):
        self.filepath_list = []

    # 批量创建目录
    def mkdirs_once_many(self, path):
        path = os.path.abspath(path) # 获取绝对路径，解决相对路径无法创建问题
        path = os.path.normpath(path)  # 去掉路径最右侧的 \\ 、/
        path = path.replace('\\', '/') # 将所有的\\转为/，避免出现转义字符串

        head, tail = os.path.split(path)
        new_dir_path = ''  # 反转后的目录路径
        root = ''  #根目录

        if not os.path.isdir(path) and os.path.isfile(path):  # 如果path指向的是文件，则继续分解文件所在目录
            head, tail = os.path.split(head)

        if tail == '':
            return

        while tail:
            new_dir_path = new_dir_path + tail + '/'
            head, tail = os.path.split(head)
            root = head
        else:
            new_dir_path = root + new_dir_path

            # 批量创建目录
            new_dir_path = os.path.normpath(new_dir_path)
            head, tail = os.path.split(new_dir_path)
            temp = ''
            while tail:
                temp = temp + '/' + tail
                dir_path = root + temp
                if not os.path.isdir(dir_path):
                    os.mkdir(dir_path)
                head, tail = os.path.split(head)


    # 获取目录下的文件
    def get_files_in_dirpath(self, dirpath):
        file_list_for_dirpath = []
        # 收集目录下的所有文件
        def collect_files_in_dirpath(dirpath):
            nonlocal file_list_for_dirpath
            if not os.path.exists(dirpath):
                logger.error('路径：%s 不存在，退出程序' % dirpath)
                exit()
            for name in os.listdir(dirpath):
                full_path = os.path.join(dirpath, name)
                if os.path.isdir(full_path):
                    collect_files_in_dirpath(full_path)
                else:
                    file_list_for_dirpath.append(full_path)
        collect_files_in_dirpath(dirpath)
        return file_list_for_dirpath

    # 复制文件或目录到指定目录(非自身目录)
    def copy_dir_or_file(self, src, dest):
        if not os.path.exists(dest):
            logger.error('目标路径：%s 不存在' % dest)
            return  [False, '目标路径：%s 不存在' % dest]
        elif not os.path.isdir(dest):
            logger.error('目标路径：%s 不为目录' % dest)
            return   [False, '目标路径：%s 不为目录' % dest]
        elif src.replace('/', '\\').rstrip('\\') == dest.replace('/', '\\').rstrip('\\'):
            logger.error('源路径和目标路径相同，无需复制')
            return [True,'源路径和目标路径相同，不需要复制']

        if not os.path.exists(src):
            logger.error('源路径：%s 不存在' % src)
            return  [False, '源路径：%s 不存在' % src]

        # /E 复制目录和子目录，包括空的 /Y 无需确认，自动覆盖已有文件
        args = 'xcopy /YE ' + os.path.normpath(src) + ' ' + os.path.normpath(dest) # 注意：xcopy不支持 d:/xxx,只支持 d:\xxxx,所以要转换
        try:
            with subprocess.Popen(args, shell=True, universal_newlines = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
                output = proc.communicate()
                logger.info('复制文件操作输出：%s' % str(output))
                if not output[1]:
                    logger.info('复制目标文件|目录(%s) 到目标目录(%s)成功' % (src, dest))
                    return [True,'复制成功']
                else:
                    logger.error('复制目标文件|目录(%s) 到目标目录(%s)失败:%s' % (src, dest, output[1]))
                    return  [False,'复制目标文件|目录(%s) 到目标目录(%s)失败:%s' % (src, dest, output[1])]
        except Exception as e:
            logger.error('复制目标文件|目录(%s) 到目标目录(%s)失败 %s' % (src, dest, e))
            return  [False, '复制目标文件|目录(%s) 到目标目录(%s)失败 %s' % (src, dest, e)]

    # 删除指定目录及其子目录下的所有子文件,不删除目录
    def delete_file(self, dirpath):
        if not os.path.exists(dirpath):
            logger.error('要删除的目标路径：%s 不存在' % dirpath)
            return  [False, '要删除的目标路径：%s 不存在' % dirpath]
        elif not os.path.isdir(dirpath):
            logger.error('要删除的目标路径：%s 不为目录' % dirpath)
            return   [False, '要删除的目标路径：%s 不为目录' % dirpath]

        # 注意：同xcopy命令，del也不支持 d:/xxxx,Linux/Unix路径的写法，只支持d:\xxx windows路径的写法
        args = 'del /F/S/Q ' + os.path.normpath(dirpath)  # /F 强制删除只读文件。 /S 删除所有子目录中的指定的文件。 /Q 安静模式。删除前，不要求确认
        try:
            with subprocess.Popen(args, shell=True, universal_newlines = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
                output = proc.communicate()
                logger.info('删除目标目录下的文件，操作输出：%s' % str(output))
                if not output[1]:
                    logger.info('删除目标目录(%s)下的文件成功' % dirpath)
                    return [True,'删除成功']
                else:
                    logger.error('删除目标目录(%s)下的文件失败：%s' % (dirpath, output[1]))
                    return [True,'删除目标目录(%s)下的文件失败：%s' % (dirpath, output[1])]
        except Exception as e:
            logger.error('删除目标目录(%s)下的文失败:%s' % (dirpath,e))
            return  [False, '删除目标目录(%s)下的文失败:%s' % (dirpath,e)]