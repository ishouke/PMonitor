# PMonitor

#### 项目介绍
基于Python结合InfluxDB及Grafana图表实时采集Linux多主机、Docker容器性能数据


#### 实现功能

无需在被监控主机上安装代理，一键对Linux远程服务器不同主机执行性能监控、性能数据采集命令，并实时展示

支持跨堡垒机收集实时性能数据(注：定制化开发，非通用，因为不同堡垒机的输入提示不一样，需要在代码里做适当更改)

支持docker容器(因为程序实现是从docker容器内部获取性能数据，所以目前仅支持 CPU,内存,I/O)



#### 使用教程

参考wiki：
https://gitee.com/ishouke/PMonitor/wikis/%E5%9F%BA%E4%BA%8EPython%E7%BB%93%E5%90%88InfluxDB%E5%8F%8AGrafana%E5%9B%BE%E8%A1%A8%E5%AE%9E%E6%97%B6%E9%87%87%E9%9B%86Linux%E5%A4%9A%E4%B8%BB%E6%9C%BA%E6%80%A7%E8%83%BD%E6%95%B0%E6%8D%AE?sort_id=766882